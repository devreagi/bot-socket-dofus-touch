# BotDT

Bot dofus touch con Fines educativos

You need **Yarn** in order to use this bot properly.

[Yarn Install](https://yarnpkg.com/lang/en/docs/install/)

## Installation

```bash
git clone https://github.com/DevReagi/BotDT.git
cd BotDT && yarn
```

## Documentation

The documentation is available on https://docs.cookietouch.com/
To update the documentation you just have to modify the /docs/ folder.

## Socials Links

- [Discord](https://discord.gg/R7HsTnD)
